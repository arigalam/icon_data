#!/bin/bash 

ADMIN="madankumar.arigala@iconplc.com"
message=$('df -h | awk  ALERT="$ALERT"
NR == 1 
$1 == "/home/arigalam:/madanICON/apache-tomcat-7.0.94/logs"
$1 == "tmp"
$1 == "/dev/"
1 {sub(/%/, " ",$5)}
$5 >= ALERT {printf "%s is almost full: %d%%\n", $1, $5}')
if [ -n "$message" ]; then
echo "$message" | mail -s "alert: Almost out of disk space" "$ADMIN"
fi

