import BaseHTTPServer

def run_while_true(server_class=BaseHTTPServer.HTTPServer, 
	handler_class=BaseHTTPServer.BaseHTTPRequestHandler): 
"""
    This assumes that keep_running() is a function of no arguments which is tested initially and after each request.  If its return value is true, the server continues. """
    server_address = ('eu-middleware-sb1', 7004)
    httpd = server_class(eu-middleware-sb1, handler_class)
    while keep_running():
        httpd.handle_request()
