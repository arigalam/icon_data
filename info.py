#!/usr/bin/python

import socket
import subprocess 
import sys
import datetime
x = datetime.datetime.now()
print(x)

#clear the screen 
subprocess.call('clear', shell=True)

#Ask for input 
remoteServer =raw_input("Enter a remote host to scan:")
remoteServerIP =socket.gethostbyname(remoteServer)  

# Print a nice banner with information on which host we are about scan
#	print "1"*60 
#	print "Please wait,scanning remote host", remoteServerIP
#	print "1"*60

# Using the range function to specify ports (here it will scans all ports between 1 and 1024)
#we also put in some error handing for catching errors 

try:  
	for port in range(1,1025):
	sock = (socket.socket(socket.AF_INET,socket.SOCK_STREAM)) 
	result = sock.connect_ex((remoteServerIP,port))
	if result == 0:
	print "Port{}:	Open".format(port)
	sock.close()  
