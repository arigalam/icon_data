#!/usr/bin/python

import subprocess, sys, os

## command to run - tcp only ##

os.system = ("netstat -p tcp -f inet")
 
## run it ##
p = subprocess.Popen(os.system, shell=True, stderr=subprocess.PIPE)
 
## But do not wait till netstat finish, start displaying output immediately ##
while True:
    out = p.stderr.read(1)
    if out == '' and p.poll() != None:
        break
    if out != '':
        sys.stdout.write(out)
        sys.stdout.flush()
