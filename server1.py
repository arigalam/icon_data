#!/usr/bin/python

import subprocess
import paramiko  
import sys
HOST = "eu-middleware-sb1"
COMMAND = "ifconfig"

def passswordless_ssh(HOST):
	ssh = subprocess.Popen(["ssh", "%s" %HOST, COMMAND],
			shell=false,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE)
	result = ssh.stdout.readline() 
	if result == []:
		error = ssh.stderr.readlines()
		print >> sys.stderr, "ERROR: %s" %error 
		return "error"
	else:
		return result 

