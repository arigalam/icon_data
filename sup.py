#!/bin/usr/python


#import subprocess
#import sys

#HOST= "eu-middleware-sb1"
# Ports are handled in ~/.ssh/config since we use OpenSSH
#COMMAND="nslookup smtp-relay.iconcr.com"

#ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND],
#                       shell=False,
#                       stdout=subprocess.PIPE,
#                       stderr=subprocess.PIPE)
#result = ssh.stdout.readlines()
#if result == []:
#    error = ssh.stderr.readlines()
#    print >>sys.stderr, "ERROR: %s" % error
#else:
#    print result


import SimpleHTTPServer
import SocketServer

PORT = 7004

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()
