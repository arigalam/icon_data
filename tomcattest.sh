#!/bin/bash

export JAVA_HOME=/usr/bin/java
export CATALINA_HOME=/home/arigalam/apache-tomcat-7.0.94
startScript=$CATALINA_HOME/bin/startup.sh

echo "Attempting to find process id for Tomcat . . ."

tomcat_pid() {
  echo `ps aux | grep org.apache.catalina.startup.Bootstrap | grep -v grep | awk '{ print $2 }'`
}
 start() {
  pid=$(tomcat_pid)
  if [ -n "$pid" ] 
  then
    echo "Tomcat is already running (pid: $pid)"
  else
    # Start tomcat
    echo "Starting tomcat"
    /bin/sh $CATALINA_HOME/bin/startup.sh
  fi

  return 0
}

#if ["$tomcat_pid" == ""]
#then
#    echo "Tomcat does not appear to be running . . . "
#else
#    echo "Killing Tomcat using process id of $tomcatPID . . ."
#    kill -9 $tomcatPID
#    echo "Waiting for process $tomcatPID to end . . ."
    while ps -ef | grep $tomcatPID ;  do sleep 1; done
    echo "Process $tomcatPID has ended . . ."
#fi
















#!/bin/bash

#export JAVA_HOME=/usr/bin/java
#export CATALINA_HOME=/home/arigalam/apache-tomcat-7.0.94
#startScript=$CATALINA_HOME/bin/startup.sh

#echo "Attempting to find process id for Tomcat . . ."
#tomcatPID=$(ps -ef | grep tomcat | grep -v restart | awk '{print $2}')

#if ["$tomcatPID" == ""]
#then 	
 #   echo "checking the status of tomcats . . ." 
#fi
